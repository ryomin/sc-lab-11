package model;

import java.util.ArrayList;

import exception.FullException;

public class Refrigerator {
	private int size;
	private ArrayList<String> things;
	
	public Refrigerator(int aSize){
		size = aSize;
		things = new ArrayList<String>();
	}
	
	public void put(String stuff) throws FullException{
		if(things.size() == size){
			throw new FullException();
		}else{
			things.add(stuff);
		}
	}
	
	public String takeOut(String stuff){
		if(things.contains(stuff)){
			return stuff;
		}else{
			return null;
			
		}
	}

	public String toString(){
		String s = "Stuff in a Refrigerator :  ";
		for(String t:things){
			s =  s +t+"  ";
		}
		return s;
	}

}
