package main;

import model.MyClass;
import exception.DataException;
import exception.FormatException;



public class MyClassMain {

	public static void main(String[] args) {
		try{
			MyClass c = new MyClass();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;	
		}catch (DataException e){
			System.out.print("D");
		}catch (FormatException e){
			System.out.print("E");
		}
	
			finally{
			System.out.print("F");
		}
		System.out.print("G");
		// TODO Auto-generated method stub

	}

}//2: AFG
 //3: ABCFG
 //4: ABCEFG
