package main;

import model.Refrigerator;
import exception.FullException;

public class RefrigeratorMain {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		Refrigerator refrig = new Refrigerator(3);
		
		try {
			refrig.put("MEAT");
			refrig.put("SHRIMP");
			refrig.takeOut("MEAT");
			refrig.put("VEGETABLE");
			refrig.put("CAKE");
			refrig.put("MILK");
			
	
			
		} catch (FullException e) {
			System.err.println("Cannot put!! Refrigerator full ");
		}finally{
			System.out.println(refrig.toString());
		}

	}


}
